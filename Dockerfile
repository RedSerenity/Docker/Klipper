FROM ubuntu:18.04 AS Builder

ARG KLIPPER_VERSION=0.10.0

RUN apt-get update
RUN apt-get install -y git

WORKDIR /klipper

RUN git clone --depth 1 --single-branch --branch v${KLIPPER_VERSION} https://github.com/KevinOConnor/klipper.git .


FROM ubuntu:18.04 AS Runtime

WORKDIR /klipper

RUN apt-get update
RUN apt-get install -y python-virtualenv virtualenv python-dev
RUN apt-get install -y build-essential libffi-dev libncurses-dev libusb-dev
RUN apt-get install -y supervisor socat
RUN apt-get clean

COPY --from=Builder /klipper /klipper

RUN virtualenv -p python2 /klippy-env
RUN /klippy-env/bin/pip install -r /klipper/scripts/klippy-requirements.txt

RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/supervisord.conf

VOLUME /config

EXPOSE 232

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

